"use strict";

var set;
function time() {
    var d = new Date();

    var hour = d.getHours();
    var minute = d.getMinutes();
    var seconds = d.getSeconds();
    minute = checkTime(minute);
    seconds = checkTime(seconds);
    document.getElementById("demo").innerHTML =
        hour + ":" + minute + ":" + seconds;
    set = setTimeout(time, 500)


}

function checkTime(i) {
    if (i < 10) {
            i = "0" + i
        }
        return i;
}

function stopTime(){
    clearTimeout(set);
}

