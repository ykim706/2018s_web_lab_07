"use strict";

function load() {
    var pages = document.getElementsByClassName("page");

    function turnPage(pageNum) {
        return function () {
            console.log("Animation" + pageNum);
            var nextPage = document.getElementById("page" + (pageNum + 1));

            nextPage.style.zIndex = 1;
        }


    }

    for (var pageNum = 0; pageNum < pages.length; pageNum++) {
        console.log(pageNum);
        var page = pages[pageNum];

        page.addEventListener("click", function () {
            console.log("click" + pageNum);
            this.classList.add("pageAnimation");
        })

        page.addEventListener("animationend", turnPage(pageNum));
    }
}

// load();